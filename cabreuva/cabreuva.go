package cabreuva

import (
	"bytes"
	"encoding/xml"
	"io/ioutil"
	"os"

	"bitbucket.org/foxsuporte/pkg-parser-notas-servico-saida/notasaida"
	"golang.org/x/net/html/charset"
)

type loteNotaSaida struct {
	caminhoXML string
}

func NewLoteNotaSaida(path string) (notasaida.LoteNotaSaida, error) {
	l := &loteNotaSaida{}
	err := l.definirArquivoXMLOrigem(path)
	if err != nil {
		return nil, err
	}
	return notasaida.LoteNotaSaida(l), nil
}

func (l *loteNotaSaida) definirArquivoXMLOrigem(path string) error {

	// Antes de começar, vamos tentar acessar o arquivo passado pelo usuário
	_, err := os.Stat(path)
	if err != nil {
		return err
	}

	l.caminhoXML = path

	return nil
}

func (l loteNotaSaida) ListarTodas() ([]notasaida.NotaSaida, error) {

	contents, err := getFileContents(l.caminhoXML)
	if err != nil {
		return []notasaida.NotaSaida{}, err
	}

	xmlStruct, err := stringToXmlStruct(contents)
	if err != nil {
		return []notasaida.NotaSaida{}, err
	}

	notas, err := xmlStructToList(xmlStruct)
	if err != nil {
		return []notasaida.NotaSaida{}, err
	}

	return notas, nil
}

func getFileContents(path string) (string, error) {
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(contents), nil
}

func stringToXmlStruct(contents string) (XML_TAG_NFE, error) {
	var notas XML_TAG_NFE
	reader := bytes.NewReader([]byte(contents))
	decoder := xml.NewDecoder(reader)
	decoder.CharsetReader = charset.NewReaderLabel
	err := decoder.Decode(&notas)
	if err != nil {
		return XML_TAG_NFE{}, err
	}
	return notas, nil
}

func xmlStructToList(n XML_TAG_NFE) ([]notasaida.NotaSaida, error) {
	notas := []notasaida.NotaSaida{}
	for _, nota := range n.NotasFiscais {
		notas = append(notas, notasaida.NotaSaida(nota))
	}
	return notas, nil
}
