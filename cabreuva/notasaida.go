package cabreuva

import "encoding/xml"

func (n NotaFiscal) DataEmissao() string {
	return n.NfeCabecario.DataEmissao
}

func (n NotaFiscal) DataExecucaoServico() string {
	return n.NfeCabecario.DataEmissao
}

func (n NotaFiscal) IsTipoTomadorPf() bool {
	return n.DadosTomador.Tipodoc != "J"
}

func (n NotaFiscal) IsTipoTomadorPj() bool {
	return n.DadosTomador.Tipodoc == "J"
}

func (n NotaFiscal) TomadorCpfCnpj() string {
	return n.DadosTomador.Documento
}

func (n NotaFiscal) TomadorEnderecoCep() string {
	return n.DadosTomador.Cep
}

func (n NotaFiscal) TomadorEnderecoCidade() string {
	return n.DadosTomador.Cidade
}

func (n NotaFiscal) TomadorEnderecoLogradouro() string {
	return n.DadosTomador.Logradouro
}

func (n NotaFiscal) TomadorIe() string {
	return n.DadosTomador.Ie
}

func (n NotaFiscal) TomadorNomeRazaoSocial() string {
	return n.DadosTomador.RazaoSocial
}

func (n NotaFiscal) ValorTotal() string {
	return n.DetalhesServico.ValorServico
}

func (n NotaFiscal) Base() string {
	return n.DetalhesServico.ValorServico
}

func (n NotaFiscal) TomadorEnderecoNumero() string {
	return n.DadosTomador.Numero
}

func (n NotaFiscal) TomadorEnderecoBairro() string {
	return n.DadosTomador.Bairro
}

func (n NotaFiscal) TomadorEnderecoComplemento() string {
	return ""
}

func (n NotaFiscal) AliquotaISS() string {
	return n.DetalhesServico.Aliquota
}

func (n NotaFiscal) NumeroNota() string {
	return n.NfeCabecario.NumeroNota
}

func (n NotaFiscal) ValorDesconto() string {
	//TODO Aqui devemos utilizar os condicionais ou incondicionais, ou ambos?
	return n.Totais.DescontoCondicional
}

func (n NotaFiscal) ValorIss() string {
	return n.Totais.ValorIss
}

type XML_TAG_NFE struct {
	XMLName      xml.Name     `xml:"nfe"`
	NotasFiscais []NotaFiscal `xml:"NotaFiscal"`
}

type NotaFiscal struct {
	NfeCabecario    NfeCabecario    `xml:"NfeCabecario"`
	DadosPrestador  DadosPrestador  `xml:"DadosPrestador"`
	DadosTomador    DadosTomador    `xml:"DadosTomador"`
	LocalServico    LocalServico    `xml:"LocalServico"`
	DetalhesServico DetalhesServico `xml:"DetalhesServico"`
	Totais          Totais          `xml:"Totais"`
	NfeRodape       NfeRodape       `xml:"NfeRodape"`
}

type NfeCabecario struct {
	Prefeitura         string `xml:"prefeitura"`
	Secretaria         string `xml:"secretaria"`
	TipoNota           string `xml:"tipoNota"`
	CodigoAutenticacao string `xml:"codigoAutenticacao"`
	Notacancelada      string `xml:"notacancelada"`
	Datacancelamento   string `xml:"datacancelamento"`
	NumeroNota         string `xml:"numeroNota"`
	DataEmissao        string `xml:"dataEmissao"`
	OptIncentivoFiscal string `xml:"optIncentivoFiscal"`
}

type DadosPrestador struct {
	TipoDocPrestador string `xml:tipoDocPrestador`
	Documento        string `xml:documento`
	Im               string `xml:im`
	RazaoSocial      string `xml:razaoSocial`
	Endereco         string `xml:endereco`
	Numero           string `xml:numero`
	Bairro           string `xml:bairro`
	Municipio        string `xml:municipio`
	Uf               string `xml:uf`
	Cep              string `xml:cep`
	Tel              string `xml:tel`
}

type DadosTomador struct {
	Tipodoc     string `xml:"tipodoc"`
	Documento   string `xml:"documento"`
	Ie          string `xml:"ie"`
	RazaoSocial string `xml:"razaoSocial"`
	Logradouro  string `xml:"logradouro"`
	Numero      string `xml:"numero"`
	Bairro      string `xml:"bairro"`
	Cidade      string `xml:"cidade"`
	Uf          string `xml:"uf"`
	Cep         string `xml:"cep"`
	Email       string `xml:"email"`
}

type LocalServico struct {
	Logradouro string `xml:"logradouro"`
	Numero     string `xml:"numero"`
	Bairro     string `xml:"bairro"`
	Uf         string `xml:"uf"`
	Cep        string `xml:"cep"`
}

type DetalhesServico struct {
	Descricao      string `xml:"descricao"`
	ValorServico   string `xml:"valorServico"`
	Codigo         string `xml:"codigo"`
	Aliquota       string `xml:"aliquota"`
	Inss           string `xml:"inss"`
	Ir             string `xml:"ir"`
	Csll           string `xml:"csll"`
	Cofins         string `xml:"cofins"`
	Pispasep       string `xml:"pispasep"`
	Outra          string `xml:"outra"`
	Issretido      string `xml:"issretido"`
	Outromunicipio string `xml:"outromunicipio"`
	Obs            string `xml:"obs"`
}

type Totais struct {
	DescontoCondicional   string `xml:"descontoCondicional"`
	DescontoIncondicional string `xml:"descontoIncondicional"`
	DeducaoMaterial       string `xml:"deducaoMaterial"`
	BaseCalculo           string `xml:"baseCalculo"`
	ValorIss              string `xml:"valorIss"`
	ValorLiquidoNota      string `xml:"valorLiquidoNota"`
}

type NfeRodape struct {
	Instrucao1 string `xml:"instrucao1"`
	Instrucao2 string `xml:"instrucao2"`
}
