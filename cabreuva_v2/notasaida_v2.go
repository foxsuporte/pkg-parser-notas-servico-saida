package cabreuva

import "encoding/xml"

func (r Reg20Item) DataEmissao() string {
	return r.DtEmiNf
}

func (r Reg20Item) DataExecucaoServico() string {
	return r.DtHrGerNf
}

func (r Reg20Item) IsTipoTomadorPf() bool {
	return r.TipoCpfCnpjTom != "J"
}

func (r Reg20Item) IsTipoTomadorPj() bool {
	return r.TipoCpfCnpjTom == "J"
}

func (r Reg20Item) TomadorCpfCnpj() string {
	return r.CpfCnpjTom
}

func (r Reg20Item) TomadorEnderecoCep() string {
	return r.CepTom
}

func (r Reg20Item) TomadorEnderecoCidade() string {
	return r.MunTom
}

func (r Reg20Item) TomadorEnderecoLogradouro() string {
	return r.LogTom
}

func (r Reg20Item) TomadorNomeRazaoSocial() string {
	return r.RazSocTom
}

func (r Reg20Item) ValorTotal() string {
	return r.VlNFS
}

func (r Reg20Item) Base() string {
	return r.VlBasCalc
}

func (r Reg20Item) TomadorEnderecoNumero() string {
	return r.NumEndTom
}

func (r Reg20Item) TomadorEnderecoBairro() string {
	return r.BairroTom
}

func (r Reg20Item) TomadorEnderecoComplemento() string {
	return ""
}

func (r Reg20Item) AliquotaISS() string {
	return r.AlqIss
}

func (r Reg20Item) NumeroNota() string {
	return r.NumNf
}

func (r Reg20Item) ValorIss() string {
	return r.VlIss
}

func (r Reg20Item) TomadorIe() string {
	return ""
}

func (r Reg20Item) ValorDesconto() string {
	return ""
}

type XML_TAG_NFE struct {
	XMLName    xml.Name `xml:"SDTNotasExport"`
	GrupoNotas Reg20    `xml:"Reg20"`
}

type Reg20 struct {
	NotasFiscais []Reg20Item `xml:"Reg20Item"`
}

type Reg20Item struct {
	TipoNf         string      `xml:"TipoNf"`
	NumNf          string      `xml:"NumNf"`
	SerNf          string      `xml:"SerNf"`
	DtEmiNf        string      `xml:"DtEmiNf"`
	DtHrGerNf      string      `xml:"DtHrGerNf	"`
	CodVernf       string      `xml:"CodVernf"`
	NumRps         string      `xml:"NumRps"`
	SerRps         string      `xml:"SerRps"`
	DtEmiRps       string      `xml:"DtEmiRps"`
	TipoCpfCnpjPre string      `xml:"TipoCpfCnpjPre"`
	CpfCnpjPre     string      `xml:"CpfCnpjPre"`
	RazSocPre      string      `xml:"RazSocPre"`
	LogPre         string      `xml:"LogPre"`
	NumEndPre      string      `xml:"NumEndPre"`
	ComplEndPre    string      `xml:"ComplEndPre"`
	BairroPre      string      `xml:"BairroPre"`
	MunPre         string      `xml:"MunPre"`
	SiglaUFPre     string      `xml:"SiglaUFPre"`
	CepPre         string      `xml:"CepPre"`
	EmailPre       string      `xml:"EmailPre"`
	TipoTribPre    string      `xml:"TipoTribPre"`
	DtAdeSN        string      `xml:"DtAdeSN"`
	AlqIssSN       string      `xml:"AlqIssSN"`
	SitNf          string      `xml:"SitNf"`
	DataCncNf      string      `xml:"DataCncNf"`
	MotivoCncNf    string      `xml:"MotivoCncNf"`
	TipoCpfCnpjTom string      `xml:"TipoCpfCnpjTom"`
	CpfCnpjTom     string      `xml:"CpfCnpjTom"`
	RazSocTom      string      `xml:"RazSocTom"`
	LogTom         string      `xml:"LogTom"`
	NumEndTom      string      `xml:"NumEndTom"`
	ComplEndTom    string      `xml:"ComplEndTom"`
	BairroTom      string      `xml:"BairroTom"`
	MunTom         string      `xml:"MunTom"`
	SiglaUFTom     string      `xml:"SiglaUFTom"`
	CepTom         string      `xml:"CepTom"`
	EMailTom       string      `xml:"EMailTom"`
	LogLocPre      string      `xml:"LogLocPre"`
	NumEndLocPre   string      `xml:"NumEndLocPre"`
	ComplEndLocPre string      `xml:"ComplEndLocPre"`
	BairroLocPre   string      `xml:"BairroLocPre"`
	MunLocPre      string      `xml:"MunLocPre"`
	SiglaUFLocpre  string      `xml:"SiglaUFLocpre"`
	CepLocPre      string      `xml:"CepLocPre"`
	CodSrv         string      `xml:"CodSrv"`
	DiscrSrv       string      `xml:"DiscrSrv"`
	VlNFS          string      `xml:"VlNFS"`
	VlDed          string      `xml:"VlDed"`
	DiscrDed       string      `xml:"DiscrDed"`
	VlBasCalc      string      `xml:"VlBasCalc"`
	AlqIss         string      `xml:"AlqIss"`
	VlIss          string      `xml:"VlIss"`
	VlIssRet       string      `xml:"VlIssRet"`
	Reg30          []Reg30Item `xml:"Reg30"`
}

type Reg30Item struct {
	TributoSigla    string `xml:"TributoSigla"`
	TributoAliquota string `xml:"TributoAliquota"`
	TributoValor    string `xml:"TributoValor"`
}
