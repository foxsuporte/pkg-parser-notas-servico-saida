package cabreuva

import (
	"testing"

	"bitbucket.org/foxsuporte/pkg-parser-notas-servico-saida/notasaida"
	"github.com/stretchr/testify/assert"
)

func TestImplements(t *testing.T) {
	// Smoke test
	_ = notasaida.LoteNotaSaida(&loteNotaSaida{})
	_ = notasaida.NotaSaida(&Reg20Item{})
}

func TestGetContents(t *testing.T) {
	contents, err := getFileContents("testdata/cabreuva_v2.xml")
	assert.Nil(t, err)
	assert.Contains(t, contents, "<Reg20>")
}

func TestParseStringToStruct(t *testing.T) {
	contents, err := getFileContents("testdata/cabreuva_v2.xml")
	assert.Nil(t, err)
	_, err = stringToXmlStruct(contents)
	assert.Nil(t, err)
}

func TestCountNotas(t *testing.T) {
	l, err := NewLoteNotaSaida("testdata/cabreuva_v2.xml")
	assert.Nil(t, err)
	notas, err := l.ListarTodas()
	assert.Nil(t, err)
	assert.Equal(t, 3, len(notas))
}

func TestPrimeiraNotaPossuiData(t *testing.T) {
	l, err := NewLoteNotaSaida("testdata/cabreuva_v2.xml")
	assert.Nil(t, err)
	notas, err := l.ListarTodas()
	assert.Nil(t, err)
	nota := notas[0]
	assert.Equal(t, "09/05/2022", nota.DataEmissao())
}
