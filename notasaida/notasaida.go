// Esse repositório lê um arquivo XML contendo lote de notas fiscais emitidas
// por meio do sistema da prefeitura de Cabreúva (compatível com novembro/2020)
// e converte para interfaces genéricas de notas fiscais de saída

// Esse package define as interfaces a serem utilizadas por clientes da biblioteca
package notasaida

type LoteNotaSaida interface {
	ListarTodas() ([]NotaSaida, error)
}

type NotaSaida interface {
	DataEmissao() string
	DataExecucaoServico() string
	TomadorCpfCnpj() string
	ValorTotal() string
	Base() string
	IsTipoTomadorPf() bool
	IsTipoTomadorPj() bool
	TomadorIe() string
	TomadorNomeRazaoSocial() string
	TomadorEnderecoCidade() string
	TomadorEnderecoCep() string
	TomadorEnderecoLogradouro() string
	TomadorEnderecoNumero() string
	TomadorEnderecoComplemento() string
	TomadorEnderecoBairro() string
	AliquotaISS() string
	NumeroNota() string
	ValorDesconto() string
}
