package rionegrinho

import (
	"testing"

	"bitbucket.org/foxsuporte/pkg-parser-notas-servico-saida/notasaida"
	"github.com/stretchr/testify/assert"
)

func TestImplements(t *testing.T) {
	// Smoke test
	_ = notasaida.LoteNotaSaida(&loteNotaSaida{})
	_ = notasaida.NotaSaida(&NFSE{})
}

func TestCountNotas(t *testing.T) {
	l, err := NewLoteNotaSaida("testdata/xmls-saida")
	assert.Nil(t, err)
	notas, err := l.ListarTodas()
	assert.Nil(t, err)
	assert.Equal(t, 2, len(notas))
}

func TestPrimeiraNotaPossuiData(t *testing.T) {
	l, err := NewLoteNotaSaida("testdata/xmls-saida")
	assert.Nil(t, err)
	notas, err := l.ListarTodas()
	assert.Nil(t, err)
	nota := notas[0]
	assert.Equal(t, "01/06/2020", nota.DataEmissao())
}
