package rionegrinho

import (
	"bytes"
	"encoding/xml"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"bitbucket.org/foxsuporte/pkg-parser-notas-servico-saida/notasaida"
	"golang.org/x/net/html/charset"
)

type loteNotaSaida struct {
	caminhoPastaXmls string
}

func NewLoteNotaSaida(path string) (notasaida.LoteNotaSaida, error) {
	l := &loteNotaSaida{}
	err := l.definirArquivoXMLOrigem(path)
	if err != nil {
		return nil, err
	}
	return notasaida.LoteNotaSaida(l), nil
}

func (l *loteNotaSaida) definirArquivoXMLOrigem(path string) error {

	// Antes de começar, vamos tentar acessar o arquivo passado pelo usuário
	_, err := os.Stat(path)
	if err != nil {
		return err
	}

	l.caminhoPastaXmls = path

	return nil
}

func (l loteNotaSaida) ListarTodas() ([]notasaida.NotaSaida, error) {
	fn, notas := fileFunc()
	fileWalk(l.caminhoPastaXmls, fn)
	return *notas, nil
}

// Individual function to run on each file
func fileFunc() (filepath.WalkFunc, *[]notasaida.NotaSaida) {
	notas := []notasaida.NotaSaida{}
	return func(path string, info os.FileInfo, err error) error {
		// We ignore subdirs
		if info.IsDir() {
			log.Println("Skipping dir", path)
			return nil
		}
		// Read the contents of path
		contents, err := ioutil.ReadFile(path)
		if err != nil {
			panic(err)
		}
		// Now that we have read the file contents, do something with it
		nota := contentsToNotaSaida(string(contents))
		notas = append(notas, notasaida.NotaSaida(nota))
		return nil
	}, &notas
}

func contentsToNotaSaida(contents string) notasaida.NotaSaida {
	var nfse NFSE
	reader := bytes.NewReader([]byte(contents))
	decoder := xml.NewDecoder(reader)
	decoder.CharsetReader = charset.NewReaderLabel
	err := decoder.Decode(&nfse)
	if err != nil {
		panic(err)
	}
	return notasaida.NotaSaida(nfse)
}

// Loop through all files, calling the individual function
func fileWalk(rootDir string, fn filepath.WalkFunc) {
	err := filepath.Walk(rootDir, fn)
	if err != nil {
		panic(err)
	}
}
