package rionegrinho

import "encoding/xml"

func (n NFSE) DataEmissao() string {
	return n.NF.DATA_NFSE
}

func (n NFSE) DataExecucaoServico() string {
	return n.NF.DATA_NFSE
}

func (n NFSE) IsTipoTomadorPf() bool {
	return n.TOMADOR.TIPO == "F"
}

func (n NFSE) IsTipoTomadorPj() bool {
	return n.TOMADOR.TIPO == "J"
}

func (n NFSE) TomadorCpfCnpj() string {
	return n.TOMADOR.CPFCNPJ
}

func (n NFSE) TomadorEnderecoCep() string {
	return n.TOMADOR.CEP
}

func (n NFSE) TomadorEnderecoCidade() string {
	return n.TOMADOR.CIDADE
}

func (n NFSE) TomadorEnderecoLogradouro() string {
	return n.TOMADOR.LOGRADOURO
}

func (n NFSE) TomadorIe() string {
	return n.TOMADOR.IE
}

func (n NFSE) TomadorNomeRazaoSocial() string {
	return n.TOMADOR.NOME_RAZAO_SOCIAL
}

func (n NFSE) ValorTotal() string {
	return n.NF.VALOR_TOTAL
}

func (n NFSE) Base() string {
	return n.ITENS.LISTAS[0].UNIDADE_VALOR_UNITARIO
}

func (n NFSE) TomadorEnderecoNumero() string {
	// TODO
	return ""
}

func (n NFSE) TomadorEnderecoBairro() string {
	return n.TOMADOR.BAIRRO
}

func (n NFSE) TomadorEnderecoComplemento() string {
	return n.TOMADOR.COMPLEMENTO
}

func (n NFSE) AliquotaISS() string {
	return n.ITENS.LISTAS[0].ALIQUOTA_ITEM_LISTA_SERVICO
}

func (n NFSE) NumeroNota() string {
	return n.NF.NUMERO_NFSE
}

func (n NFSE) ValorDesconto() string {
	//TODO Aqui devemos utilizar os condicionais ou incondicionais, ou ambos?
	return n.NF.VALOR_DESCONTO
}

type NFSE struct {
	XMLName   xml.Name  `xml:"nfse"`
	NF        NF        `xml:"nf"`
	PRESTADOR PRESTADOR `xml:"prestador"`
	TOMADOR   TOMADOR   `xml:"tomador"`
	ITENS     ITENS     `xml:"itens"`
}

type NF struct {
	NUMERO_NFSE               string `xml:"numero_nfse"`
	SERIE_NFSE                string `xml:"serie_nfse"`
	DATA_NFSE                 string `xml:"data_nfse"`
	HORA_NFSE                 string `xml:"hora_nfse"`
	VALOR_TOTAL               string `xml:"valor_total"`
	VALOR_DESCONTO            string `xml:"valor_desconto"`
	VALOR_IR                  string `xml:"valor_ir"`
	VALOR_INSS                string `xml:"valor_inss"`
	VALOR_CONTRIBUICAO_SOCIAL string `xml:"valor_contribuicao_social"`
	VALOR_RPS                 string `xml:"valor_rps"`
	VALOR_PIS                 string `xml:"valor_pis"`
	VALOR_COFINS              string `xml:"valor_cofins"`
	OBSERVACAO                string `xml:"observacao"`
}

type PRESTADOR struct {
	CPFCNPJG string `xml:"cpfcnpjg"`
	CIDADEG  string `xml:"cidadeg"`
}

type TOMADOR struct {
	TIPO                    string `xml:"tipo"`
	CPFCNPJ                 string `xml:"cpfcnpj"`
	IE                      string `xml:"ie"`
	NOME_RAZAO_SOCIAL       string `xml:"nome_razao_social"`
	SOBRENOME_NOME_FANTASIA string `xml:"sobrenome_nome_fantasia"`
	LOGRADOURO              string `xml:"logradouro"`
	EMAIL                   string `xml:"email"`
	COMPLEMENTO             string `xml:"complemento"`
	PONTO_REFERENCIA        string `xml:"ponto_referencia"`
	BAIRRO                  string `xml:"bairro"`
	CIDADE                  string `xml:"cidade"`
	CEP                     string `xml:"cep"`
	DDD_FONE_COMERCIAL      string `xml:"ddd_fone_comercial"`
	FONE_COMERCIAL          string `xml:"fone_comercial"`
	DDD_FONE_RESIDENCIAL    string `xml:"ddd_fone_residencial"`
	FONE_RESIDENCIAL        string `xml:"fone_residencial"`
	DDD_FAX                 string `xml:"ddd_fax"`
	FONE_FAX                string `xml:"fone_fax"`
}

type ITENS struct {
	LISTAS []LISTA `xml:"lista"`
}

type LISTA struct {
	CODIGO_LOCAL_PRESTACAO_SERVICO string `xml:"codigo_local_prestacao_servico"`
	CODIGO_ITEM_LISTA_SERVICO      string `xml:"codigo_item_lista_servico"`
	DESCRITIVO                     string `xml:"descritivo"`
	ALIQUOTA_ITEM_LISTA_SERVICO    string `xml:"aliquota_item_lista_servico"`
	SITUACAO_TRIBUTARIA            string `xml:"situacao_tributaria"`
	VALOR_TRIBUTAVEL               string `xml:"valor_tributavel"`
	VALOR_DEDUCAO                  string `xml:"valor_deducao"`
	VALOR_ISSRF                    string `xml:"valor_issrf"`
	TRIBUTA_MUNICIPIO_PRESTADOR    string `xml:"tributa_municipio_prestador"`
	UNIDADE_CODIGO                 string `xml:"unidade_codigo"`
	UNIDADE_QUANTIDADE             string `xml:"unidade_quantidade"`
	UNIDADE_VALOR_UNITARIO         string `xml:"unidade_valor_unitario"`
}
