package saopaulo

import (
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"bitbucket.org/foxsuporte/pkg-parser-notas-servico-saida/notasaida"
)

type loteNotaSaida struct {
	caminhoXML string
}

func NewLoteNotaSaida(path string) (notasaida.LoteNotaSaida, error) {
	l := &loteNotaSaida{}
	err := l.definirArquivoXMLOrigem(path)
	if err != nil {
		return nil, err
	}
	return notasaida.LoteNotaSaida(l), nil
}

func (l *loteNotaSaida) definirArquivoXMLOrigem(path string) error {

	// Antes de começar, vamos tentar acessar o arquivo passado pelo usuário
	_, err := os.Stat(path)
	if err != nil {
		return err
	}

	l.caminhoXML = path

	return nil
}

func (l loteNotaSaida) ListarTodas() ([]notasaida.NotaSaida, error) {

	contents, err := getFileContents(l.caminhoXML)
	if err != nil {
		return []notasaida.NotaSaida{}, err
	}

	notas, err := stringToStructList(contents)
	if err != nil {
		return []notasaida.NotaSaida{}, err
	}

	return notas, nil
}

func getFileContents(path string) (string, error) {
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(contents), nil
}

func stringToStructList(contents string) ([]notasaida.NotaSaida, error) {
	notas := []notasaida.NotaSaida{}
	r := csv.NewReader(strings.NewReader(contents))
	r.Comma = ';'
	for {
		line, err := r.Read()
		if err == io.EOF {
			break
		}
		if line[0] != "Tipo de Registro" {
			nota := csvSpToNfse(line)
			notas = append(notas, notasaida.NotaSaida(nota))
		}
	}
	return notas, nil
}

func csvSpToNfse(line []string) NFSE {
	nfse := NFSE{}
	fmt.Printf("%+v\n", line)
	nfse.NF.NUMERO_NFSE = line[1]
	nfse.NF.SERIE_NFSE = line[5]
	nfse.NF.DATA_NFSE = line[2]
	nfse.NF.HORA_NFSE = line[2]
	nfse.NF.VALOR_TOTAL = line[26]
	nfse.NF.VALOR_IR = line[58]
	nfse.NF.VALOR_INSS = line[57]
	nfse.NF.VALOR_CONTRIBUICAO_SOCIAL = line[59]
	nfse.NF.VALOR_PIS = line[55]
	nfse.NF.VALOR_COFINS = line[56]
	nfse.PRESTADOR.CPFCNPJG = line[10]
	nfse.PRESTADOR.CIDADEG = line[17]
	if line[33] == "2" {
		nfse.TOMADOR.TIPO = "J"
	} else {
		nfse.TOMADOR.TIPO = "F"
	}
	nfse.TOMADOR.CPFCNPJ = line[34]
	nfse.TOMADOR.IE = line[36]
	nfse.TOMADOR.NOME_RAZAO_SOCIAL = line[37]
	nfse.TOMADOR.SOBRENOME_NOME_FANTASIA = line[37]
	nfse.TOMADOR.LOGRADOURO = line[38] + " " + line[39] + ", " + "40"
	nfse.TOMADOR.EMAIL = line[46]
	nfse.TOMADOR.COMPLEMENTO = line[41]
	nfse.TOMADOR.BAIRRO = line[42]
	nfse.TOMADOR.CIDADE = line[43]
	nfse.TOMADOR.CEP = line[45]

	nfse.ITENS.LISTAS = []LISTA{
		{
			CODIGO_LOCAL_PRESTACAO_SERVICO: "3550308",
			CODIGO_ITEM_LISTA_SERVICO:      line[28],
			DESCRITIVO:                     line[72],
			ALIQUOTA_ITEM_LISTA_SERVICO:    line[29],
			SITUACAO_TRIBUTARIA:            "0",
			VALOR_TRIBUTAVEL:               line[26],
			VALOR_DEDUCAO:                  "0",
			VALOR_ISSRF:                    "0",
			TRIBUTA_MUNICIPIO_PRESTADOR:    "S",
			UNIDADE_CODIGO:                 "1",
			UNIDADE_QUANTIDADE:             "1",
			UNIDADE_VALOR_UNITARIO:         line[26],
		},
	}
	return nfse
}
