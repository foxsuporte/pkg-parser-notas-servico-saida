package saopaulo

func (n NFSE) DataEmissao() string {
	return n.NF.DATA_NFSE
}

func (n NFSE) DataExecucaoServico() string {
	return n.NF.DATA_NFSE
}

func (n NFSE) IsTipoTomadorPf() bool {
	return n.TOMADOR.TIPO == "F"
}

func (n NFSE) IsTipoTomadorPj() bool {
	return n.TOMADOR.TIPO == "J"
}

func (n NFSE) TomadorCpfCnpj() string {
	return n.TOMADOR.CPFCNPJ
}

func (n NFSE) TomadorEnderecoCep() string {
	return n.TOMADOR.CEP
}

func (n NFSE) TomadorEnderecoCidade() string {
	return n.TOMADOR.CIDADE
}

func (n NFSE) TomadorEnderecoLogradouro() string {
	return n.TOMADOR.LOGRADOURO
}

func (n NFSE) TomadorIe() string {
	return n.TOMADOR.IE
}

func (n NFSE) TomadorNomeRazaoSocial() string {
	return n.TOMADOR.NOME_RAZAO_SOCIAL
}

func (n NFSE) ValorTotal() string {
	return n.NF.VALOR_TOTAL
}

func (n NFSE) Base() string {
	return n.ITENS.LISTAS[0].UNIDADE_VALOR_UNITARIO
}

func (n NFSE) TomadorEnderecoNumero() string {
	// TODO
	return ""
}

func (n NFSE) TomadorEnderecoBairro() string {
	return n.TOMADOR.BAIRRO
}

func (n NFSE) TomadorEnderecoComplemento() string {
	return n.TOMADOR.COMPLEMENTO
}

func (n NFSE) AliquotaISS() string {
	return n.ITENS.LISTAS[0].ALIQUOTA_ITEM_LISTA_SERVICO
}

func (n NFSE) NumeroNota() string {
	return n.NF.NUMERO_NFSE
}

func (n NFSE) ValorDesconto() string {
	//TODO Aqui devemos utilizar os condicionais ou incondicionais, ou ambos?
	return n.NF.VALOR_DESCONTO
}

type NFSE struct {
	NF        NF
	PRESTADOR PRESTADOR
	TOMADOR   TOMADOR
	ITENS     ITENS
}

type NF struct {
	NUMERO_NFSE               string
	SERIE_NFSE                string
	DATA_NFSE                 string
	HORA_NFSE                 string
	VALOR_TOTAL               string
	VALOR_DESCONTO            string
	VALOR_IR                  string
	VALOR_INSS                string
	VALOR_CONTRIBUICAO_SOCIAL string
	VALOR_RPS                 string
	VALOR_PIS                 string
	VALOR_COFINS              string
	OBSERVACAO                string
}

type PRESTADOR struct {
	CPFCNPJG string
	CIDADEG  string
}

type TOMADOR struct {
	TIPO                    string
	CPFCNPJ                 string
	IE                      string
	NOME_RAZAO_SOCIAL       string
	SOBRENOME_NOME_FANTASIA string
	LOGRADOURO              string
	EMAIL                   string
	COMPLEMENTO             string
	PONTO_REFERENCIA        string
	BAIRRO                  string
	CIDADE                  string
	CEP                     string
	DDD_FONE_COMERCIAL      string
	FONE_COMERCIAL          string
	DDD_FONE_RESIDENCIAL    string
	FONE_RESIDENCIAL        string
	DDD_FAX                 string
	FONE_FAX                string
}

type ITENS struct {
	LISTAS []LISTA
}

type LISTA struct {
	CODIGO_LOCAL_PRESTACAO_SERVICO string
	CODIGO_ITEM_LISTA_SERVICO      string
	DESCRITIVO                     string
	ALIQUOTA_ITEM_LISTA_SERVICO    string
	SITUACAO_TRIBUTARIA            string
	VALOR_TRIBUTAVEL               string
	VALOR_DEDUCAO                  string
	VALOR_ISSRF                    string
	TRIBUTA_MUNICIPIO_PRESTADOR    string
	UNIDADE_CODIGO                 string
	UNIDADE_QUANTIDADE             string
	UNIDADE_VALOR_UNITARIO         string
}
