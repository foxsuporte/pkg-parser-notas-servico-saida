package saopaulo

import (
	"testing"

	"bitbucket.org/foxsuporte/pkg-parser-notas-servico-saida/notasaida"
	"github.com/stretchr/testify/assert"
)

func TestImplements(t *testing.T) {
	// Smoke test
	_ = notasaida.LoteNotaSaida(&loteNotaSaida{})
	_ = notasaida.NotaSaida(&NFSE{})
}

func TestGetContents(t *testing.T) {
	contents, err := getFileContents("testdata/saopaulo.csv")
	assert.Nil(t, err)
	assert.Contains(t, contents, "Tipo de Registro")
}

func TestParseStringToList(t *testing.T) {
	contents, err := getFileContents("testdata/saopaulo.csv")
	assert.Nil(t, err)
	_, err = stringToStructList(contents)
	assert.Nil(t, err)
}

func TestCountNotas(t *testing.T) {
	l, err := NewLoteNotaSaida("testdata/saopaulo.csv")
	assert.Nil(t, err)
	notas, err := l.ListarTodas()
	assert.Nil(t, err)
	assert.Equal(t, 2, len(notas))
}

func TestPrimeiraNotaPossuiData(t *testing.T) {
	l, err := NewLoteNotaSaida("testdata/saopaulo.csv")
	assert.Nil(t, err)
	notas, err := l.ListarTodas()
	assert.Nil(t, err)
	nota := notas[0]
	assert.Equal(t, "01/09/2020", nota.DataEmissao())
}
